package sqliteutility.sql;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SQLUtilityTest
{

    @Test
    public void tr()
    {
        assertEquals("NULL", SQLUtility.tr(null));
        assertEquals("''", SQLUtility.tr("\n  "));
        assertEquals("'it''s dangerous'", SQLUtility.tr("it's dangerous "));
        assertEquals("'abc''\t''def'", SQLUtility.tr("\n\t abc'\t'def\n"));
    }

    @Test
    public void getRowCondition()
    {

    }

    @Test
    public void deleteColumnCommand()
    {
    }

    @Test
    public void updateCommand()
    {
    }

    @Test
    public void renameTableCommand()
    {
        assertEquals("ALTER TABLE 'one' RENAME TO 'two'",
                SQLUtility.renameTableCommand("one", "two"));
        assertEquals("ALTER TABLE 'xxx xxx' RENAME TO 'x x'",
                SQLUtility.renameTableCommand("xxx xxx", "x x"));
    }

    @Test
    public void insertCommand()
    {
        assertEquals("INSERT INTO 'xyz' VALUES ()",
                SQLUtility.insertCommand("xyz"));
        assertEquals("INSERT INTO 'xyz' VALUES ('0.5','10','abc',NULL)",
                SQLUtility.insertCommand("xyz", 0.5, 10, "abc", null));
    }

    @Test
    public void trspace()
    {
        assertEquals("hello, world", SQLUtility.xargs("hello,   world"));
        assertEquals("xxx yyy zzz", SQLUtility.xargs("\nxxx  yyy    zzz\t "));
        assertEquals("a b c", SQLUtility.xargs("a  \r  b\t   \n c\n"));
    }
}
