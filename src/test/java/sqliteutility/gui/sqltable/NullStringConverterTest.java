package sqliteutility.gui.sqltable;

import org.junit.*;
import static org.junit.Assert.*;

public class NullStringConverterTest
{
    @Test
    public void convertToString() {
        NullStringConverter converter = new NullStringConverter();
        assertEquals("null", converter.toString(null));
        assertEquals("", converter.toString(""));
        assertEquals("10", converter.toString(10));
        assertEquals("ABC", converter.toString("ABC"));
        assertEquals("true", converter.toString(true));
        assertEquals("3.5", converter.toString(3.5));
    }

    @Test
    public void convertFromString() {
        NullStringConverter converter = new NullStringConverter();
        assertEquals(null, converter.fromString("null"));
        assertEquals("", converter.fromString(""));
        assertEquals("10", converter.fromString("10"));
        assertEquals("ABC", converter.fromString("ABC"));
        assertEquals("true", converter.fromString("true"));
        assertEquals("3.5", converter.fromString("3.5"));
    }
}
