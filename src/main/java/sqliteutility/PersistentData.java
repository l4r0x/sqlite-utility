/*
 * Copyright (C) 2016 Lars Wrenger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sqliteutility;

import java.io.File;
import java.util.*;
import java.util.prefs.Preferences;

/**
 * Utility for loading and saving user preferences.
 *
 * @author Lars Wrenger
 */
public class PersistentData
{

    public static final int RECENT_FILES_COUNT = 5;
    private static final String ROOT = "sqliteutility";
    private static final String KEY_RECENT_FILES = "recentFiles";

    /**
     * Loads the most recently opened files from the user preferences.
     *
     * @return the most recently opened files (at most <code>RECENT_FILES_COUNT</code>)
     */
    public static List<File> loadLastFiles()
    {
        Preferences pref = Preferences.userRoot().node(ROOT);

        List<File> lastOpened = new ArrayList<>();
        for (int i = 0; i < RECENT_FILES_COUNT; i++) {
            String item = pref.get(KEY_RECENT_FILES + i, null);
            if (item == null) break;

            File file = new File(item);
            if (file.exists() && !lastOpened.contains(file)) {
                lastOpened.add(file);
            }
        }
        return lastOpened;
    }

    /**
     * Stores the most recently opened files into the user preferences.
     *
     * @param lastFiles the most recently opened files
     */
    public static void saveLastFiles(Collection<File> lastFiles)
    {
        Preferences pref = Preferences.userRoot().node(ROOT);

        int i = 0;
        for (File file : lastFiles) {
            if (i >= RECENT_FILES_COUNT) break;

            pref.put(KEY_RECENT_FILES + i, file.getAbsolutePath());
            i++;
        }
    }

    public static File parseFile(String arg0)
    {
        File file = new File(arg0);
        if (!file.exists()) return null;
        return file;
    }
}
