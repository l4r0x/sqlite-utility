/*
 * Copyright (C) 2016 Lars Wrenger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sqliteutility;

import javafx.application.Application;
import javafx.stage.Stage;
import sqliteutility.gui.*;

import java.io.*;
import java.util.*;

/**
 * Main Application class. Starts the JavaFX Application Thread ({@link Application}) and shows the
 * main window.
 *
 * @author Lars Wrenger
 */
public class App
        extends Application
{
    public static final String LANGUAGE_BUNDLES = "sqliteutility.bundles.sqliteutility";

    public static MainController win;
    public static File startFile;

    /**
     * Starts the Javafx main threat.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage stage)
            throws Exception
    {
        handleOpenFile(getParameters().getUnnamed());

        setUserAgentStylesheet(STYLESHEET_MODENA);

        ResourceBundle translation = getTranslation();

        try {
            if (startFile != null)
                win = new MainController(stage, translation, startFile);
            else
                win = new MainController(stage, translation);
            win.show();
        }
        catch (IOException e) {
            AlertUtility.exceptionDialog(translation.getString("error.win"), e);
        }
    }

    private ResourceBundle getTranslation() {
        Locale language = (Locale.getDefault().getLanguage().equals(
                Locale.GERMAN.getLanguage())) ? Locale.GERMAN : Locale.ENGLISH;
        return ResourceBundle.getBundle(LANGUAGE_BUNDLES, language);
    }


    /**
     * Handles the File Opening by Commandline Arguments or by an Event Handler on MacOS.
     *
     * @param args Commandline Arguments
     */
    private static void handleOpenFile(List<String> args)
    {
        try {
            if (!args.isEmpty()) {
                File file = PersistentData.parseFile(args.get(0));
                if (file != null) startFile = file;
            }
        }
        catch (Exception e) {
            AlertUtility.exceptionDialog("Unable to open file.", e);
        }
    }

    @Override
    public void stop()
            throws Exception
    {
        super.stop();
        if (win != null)
            PersistentData.saveLastFiles(win.getRecentFiles());
    }
}
