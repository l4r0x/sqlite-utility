/*
 * Copyright (C) 2016 Lars Wrenger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sqliteutility.gui;

import javafx.collections.*;
import javafx.scene.control.*;
import sqliteutility.PersistentData;

import java.io.File;
import java.util.*;

/**
 * Module for handling recent files.
 *
 * @author Lars Wrenger
 */
public class RecentFilesMenu
{
    /**
     * EventHandler, that is called when a MenuItem is selected.
     */
    public interface SelectFileHandler
    {
        /**
         * Called when a MenuItem is selected.
         *
         * @param file The selected file.
         */
        void call(File file);
    }

    private final ObservableList<File> recentFiles = FXCollections.observableArrayList();
    private final SelectFileHandler onSelectFile;
    private final Menu menuComponent;

    /**
     * Creates a new recent files menu and attaches it to a menu component.
     *
     * @param menuComponent The GUI Component to display the recent files
     * @param fileAction  EventHandler, that is called when a MenuItem is selected.
     */
    public RecentFilesMenu(
            Menu menuComponent, SelectFileHandler fileAction)
    {
        this.menuComponent = menuComponent;
        this.onSelectFile = fileAction;
    }

    /**
     * Inserts a new file at the front of the list. The first item, that refers to the currently
     * open file, isn't
     * displayed on the menu.
     *
     * @param file A new file.
     */
    public void open(File file)
    {
        recentFiles.remove(file);
        recentFiles.add(0, file);
        if (recentFiles.size() >= PersistentData.RECENT_FILES_COUNT)
            recentFiles.remove(PersistentData.RECENT_FILES_COUNT, recentFiles.size());
        update();
    }

    /**
     * Clears the list and open the given elements instead.
     *
     * @param recentFiles the five most recently opened files.
     */
    public void setAll(Collection<File> recentFiles)
    {
        this.recentFiles.setAll(recentFiles);
        update();
    }

    /**
     * Updates the MenuItems to all recently opened files without the currently open file.
     */
    private void update()
    {
        menuComponent.getItems().setAll(generateMenu(recentFiles));
        menuComponent.setDisable(menuComponent.getItems().size() <= 0);
    }

    private MenuItem[] generateMenu(Collection<File> files) {
        return files.stream()
                .map(file -> {
                    MenuItem item = new MenuItem(file.getName());
                    item.setOnAction(event -> onItemSelected(file));
                    return item;
                })
                .toArray(MenuItem[]::new);
    }

    private void onItemSelected(File file)
    {
        onSelectFile.call(file);
        open(file);
    }

    public File current()
    {
        return get(0);
    }

    public File get(int index)
    {
        if (index < 0 || index >= recentFiles.size()) return null;
        return recentFiles.get(index);
    }

    /**
     * Returns all most recently opened files.
     *
     * @return the most recently opened files.
     */
    public List<File> toList()
    {
        return Collections.unmodifiableList(recentFiles);
    }

}
