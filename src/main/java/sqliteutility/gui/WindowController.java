package sqliteutility.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ResourceBundle;

/**
 * Base class for all types of windows.
 */
abstract public class WindowController
{
    protected final Stage stage;
    protected final ResourceBundle resources;

    /**
     * Initializes a new window and loads its content.
     *
     * @param stage     Stage of the window
     * @param resources Translations
     *
     * @throws IOException when the loading fails
     */
    protected WindowController(Stage stage, ResourceBundle resources)
            throws IOException
    {
        this.stage = stage;
        this.resources = resources;

        loadContent();
    }

    private void loadContent()
            throws IOException
    {
        FXMLLoader loader = new FXMLLoader(WindowController.class.getResource(getUrl()), resources);
        loader.setController(this);

        Parent root = loader.load();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setOnShown(event -> calcMinSize());
    }


    abstract protected String getUrl();

    /**
     * Shows this window.
     */
    public void show()
    {
        stage.show();
    }

    /**
     * Closes this window.
     */
    public void close()
    {
        stage.close();
    }

    /**
     * Displaces the window to prevent hiding other windows.
     */
    public void displace()
    {
        stage.setX(stage.getX() + 20);
        stage.setY(stage.getY() + 20);
    }

    /**
     * Calculates the minimum Stage size with the size of the contained scene.
     * Should be called immediately after showing the stage.
     * <p>
     * Usage: <code>stage.setOnShown((event) -&gt; calcMinSize())</code>
     */
    protected void calcMinSize()
    {
        stage.setMinWidth(stage.getScene().getRoot().minWidth(-1)
                + (stage.getWidth() - stage.getScene().getWidth()));

        stage.setMinHeight(stage.getScene().getRoot().minHeight(-1)
                + (stage.getHeight() - stage.getScene().getHeight()));
    }
}