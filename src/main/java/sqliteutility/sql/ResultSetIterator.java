package sqliteutility.sql;

import java.sql.*;
import java.util.Iterator;

class ResultSetIterator
        implements Iterator<Object[]>
{
    private final ResultSet result;
    private final int columns;

    private boolean peeked = false;
    private boolean hasNext;

    ResultSetIterator(ResultSet result)
            throws SQLException
    {
        this.result = result;
        columns = result.getMetaData().getColumnCount();
    }

    @Override
    public boolean hasNext()
    {
        if (peeked) return hasNext;

        try {
            peeked = true;
            return hasNext = result.next();
        }
        catch (SQLException e) {
            return false;
        }
    }

    @Override
    public Object[] next()
    {
        if (!peeked) hasNext();
        if (!hasNext) return null;
        peeked = false;

        Object[] values = new Object[columns];
        try {
            for (int i = 0; i < columns; i++) {
                values[i] = result.getObject(i + 1);
            }
        }
        catch (SQLException ignored) {}
        return values;
    }
}
