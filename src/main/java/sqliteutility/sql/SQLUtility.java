/*
 * Copyright (C) 2016 Lars Wrenger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sqliteutility.sql;

import javafx.collections.ObservableList;
import javafx.scene.control.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Helper class to generate and evaluate sql requests.
 *
 * @author Lars Wrenger
 */
public class SQLUtility
{

    private SQLUtility() {}

    /**
     * Return a trimmed input with escaped ' characters.
     *
     * @param input user input
     *
     * @return if null then "NULL" otherwise the trimmed input surrounded by apostrophes
     */
    public static String tr(String input)
    {
        return input == null ? "NULL" : ("'" + input.trim().replace("'", "''") + "'");
    }

    /**
     * Generates a condition to select all rows with the given unique ids.
     * @param ids the ids to be selected
     * @return 'WHERE' condition
     */
    public static String getIdCondition(int... ids) {
        StringJoiner condition = new StringJoiner(" OR ", "WHERE ", "");
        for (int id : ids) {
            condition.add("_ROWID_ = " + id);
        }
        return condition.toString();
    }

    /**
     * Generates a sql request to delete the selected column.
     *
     * @param position  tablePosition
     * @param currTable the sql table to be edited
     *
     * @return sql request
     */
    public static String deleteColumnCommand(
            TablePosition<ObservableList<Object>, Object> position, String currTable)
    {
        String consistentCols = position.getTableView().getColumns().stream()
                .map(TableColumnBase::getText)
                .filter(c -> !c.equals(position.getTableColumn().getText()))
                .collect(Collectors.joining(","));

        return String.format("BEGIN TRANSACTION;"
                + "CREATE TEMPORARY TABLE %1$s_backup(%2$s);"
                + "INSERT INTO %1$s_backup SELECT %2$s FROM %1$s;"
                + "DROP TABLE %1$s;"
                + "CREATE TABLE %1$s(%2$s);"
                + "INSERT INTO %1$s SELECT %2$s FROM %1$s_backup;"
                + "DROP TABLE %1$s_backup;"
                + "COMMIT;", currTable, consistentCols);
    }

    /**
     * Generates a sql request to update the sql table after editing.
     *
     * @param currTable the sql table to be updated
     *
     * @return sql request
     */
    public static String updateCommand(
            String currTable, String value, String column, String rowCondition)
    {
        return "UPDATE " + currTable + " SET " + column + "=" + tr(value) + " " + rowCondition;
    }

    /**
     * Generates a sql request to rename the selected table.
     *
     * @param name    current table name
     * @param newName new name
     *
     * @return sql request
     */
    public static String renameTableCommand(String name, String newName)
    {
        return "ALTER TABLE " + tr(name) + " RENAME TO " + tr(newName);
    }

    /**
     * Generates a sql request to insert a new row.
     *
     * @param table  the sql table to be edited
     * @param values the values of the new row
     *
     * @return sql request
     */
    public static String insertCommand(String table, Object... values)
    {
        return "INSERT INTO " + tr(table) + " VALUES ("
                + Arrays.stream(values)
                    .map(o -> tr(o == null ? null : String.valueOf(o)))
                    .collect(Collectors.joining(","))
                + ")";
    }

    public static String xargs(String val)
    {
        return val.trim().replaceAll("\\s", " ")
                .replaceAll(" {2,}+", " ");
    }

    public static String sqliteSchema()
    {
        return "select name,sql from sqlite_master where type='table' order by name";
    }

    public static String selectTable(String table) {
        return "SELECT _ROWID_ ,* FROM " + table;
    }

    public static String deleteWithId(String table, int... ids) {
        return "DELETE FROM " + table + " " + SQLUtility.getIdCondition(ids);
    }

    public static String addColumn(String table, String name, String options) {
        // TODO: Validate!!!
        return "ALTER TABLE " + table + " ADD " + name + " " + options;
    }
}
